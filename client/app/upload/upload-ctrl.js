// upload.controller.js

(function(){

    angular
        .module("UploadApp")
        .controller("UploadCtrl", UploadCtrl);

    UploadCtrl.$inject = ["Upload"];

    function UploadCtrl(Upload) {

        var vm = this;

        vm.imgFile = null;
        vm.status = {
            message: "",
            code: 0
        };

        vm.upload = upload;  // expose function to vm

        function upload(){
            console.log("CTRL upload() >> ");

            Upload
            .upload({     // SVC
                url:'/upload',
                data: {
                    "img-file": vm.imgFile,
                }
            })
            .then(function(result) {
                vm.fileurl = result.data;
                vm.status.message = "Upload CTRL >> Success";
                vm.status.code = 202;            
            })
            .catch(function(err){
                console.log(err);
                vm.status.message = "Upload CTRL >> Error";
                vm.status.code = 500;
            })
        }
        
    } // end of Controller
    
}) ();
